class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.string :url
      t.string :summary
			t.string :title

      t.timestamps
    end
  end
end
