# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :content_tag do
    content_url "MyString"
    tag_name "MyString"
  end
end
