# encoding: utf-8
require 'json'
require 'rubygems'
require 'igo-ruby'

tagger = Igo::Tagger.new('./ipadic')
ignore_type = %(助詞 助動詞)
ignore_subtype = %(数 名詞,サ変接続 名詞,非自立)


file = File.read('codezine.json')
file = file.force_encoding("UTF-8")
json = JSON.parser.new(file)
hash =  json.parse()

parsed = hash['hits']
parsed = parsed['hits']

  parsed.each do |article|
    word_count = {}
    source = article['_source']
    title = source['title']
    url = source['url']

    t = tagger.parse(title)
    cell = [url] 

 	t.each do |m|

          next if m.surface.length <= 1
	  next if ignore_type.include?(m.feature.split(',').slice(0,1).join(','))
	  next if ignore_subtype.include?(m.feature.split(',').slice(0,2).join(','))

	  word_count[m.surface] = 0 unless word_count[m.surface]
	  word_count[m.surface] += 1

	end
	
	word_count.each do |txt|
		cell.insert(1,txt.join(' | '))
	end
     
	puts cell.join(',')

  end
