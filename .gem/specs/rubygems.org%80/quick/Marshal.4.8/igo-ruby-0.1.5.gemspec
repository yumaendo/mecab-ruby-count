u:Gem::Specification�["
1.5.0i"igo-rubyU:Gem::Version["
0.1.5u:	Time���    "6Ruby port of Igo Japanese morphological analyzer.U:Gem::Requirement[[[">=U; ["0U;[[[">U; ["
1.3.6"	ruby[	o:Gem::Dependency
:@requirementU;[[["~>U; ["
2.1.0:@prereleaseF:@version_requirements@ :
@name"
rspec:
@type:developmento;
;	U;[[["~>U; ["
1.0.0;
F;@*;"bundler;;o;
;	U;[[["~>U; ["
1.5.1;
F;@4;"jeweler;;o;
;	U;[[[">=U; ["0;
F;@>;"	rcov;;0"24signals@gmail.com["K.Nishi"�
    Ruby port of Igo Japanese morphological analyzer. Igo-ruby needs Igo's binary dictionary files.
    These files created by Java programs.
    See: http://igo.sourceforge.jp/
  "$http://github.com/kyow/igo-rubyT@["MIT